document.addEventListener("deviceready", function() {
    var $ball = $("#ball");
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    var ballHeight = 50;
    var ballWidth = 50;
    var dx = 0;
    var dy = 0;
    var position = {};
    var leftOffset = windowWidth / 2 - ballWidth / 2
    var topOffset = windowHeight / 2 - ballHeight / 2;
    $ball.css("top", topOffset + "px");
    $ball.css("left", leftOffset + "px");
    var accelerometerSuccess = function(acceleration) {
        setTimeout(function() {
            //wartości od -10 do 10
            dx += acceleration.x;
            dy += acceleration.y;
            position = $ball.position();
            if (position.top >= 0 && position.top <= windowHeight - ballHeight) {
                $ball.css("top", (topOffset + dx) + "px");
            } else {
                if (position.top < 0) {
                    topOffset = 0;
                    $ball.css("top", topOffset + "px");
                } else {
                    topOffset = windowHeight - ballHeight;
                    $ball.css("top", topOffset + "px");
                }
                dx = 0;
            }
            if (position.left >= 0 && position.left <= windowWidth - ballWidth) {
                $ball.css("left", (leftOffset + dy) + "px");
            } else {
                if (position.left < 0) {
                    leftOffset = 0;
                    $ball.css("left", leftOffset + "px");
                } else {
                    leftOffset = windowWidth - ballWidth;
                    $ball.css("left", leftOffset + "px");
                }
                dy = 0;
            }
        }, 200);
    };
    var onError = function() {
        alert('Coś poszło nie tak!');
    };
    var accelerometerOptions = {
        frequency: 10
    };
    navigator.accelerometer.watchAcceleration(accelerometerSuccess, onError, accelerometerOptions);
}, false);
